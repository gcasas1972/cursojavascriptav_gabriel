class Cuenta {
  constructor(numero, saldo) {
      if (new.target === Cuenta) {
        throw new Error( 'esta es una clase abstracta' );
      }
    this.numero=numero;
    this.saldo = saldo;
  }
  acreditar(monto){
    this.saldo+=monto;
  }
  debitar(monto){
    throw new Error("Method 'debitar' debe ser implementado.");
  }
  toString(){
    return `<br/>numero=${this.numero}
            <br/>saldo= ${this.saldo}`;
  }


}
