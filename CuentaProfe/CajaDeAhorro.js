class CajaDeAhorro extends Cuenta {
  constructor(numero, saldo, interes) {
    super(numero, saldo);
    this.interes = interes;
  }
  debitar(monto){
    if(monto<=this.saldo)
      this.saldo-=monto;
    else
      throw new Error( 'Saldo insuficiente' );

  }
  toString(){
    return `${super.toString()}
          <br/>interes = ${this.interes}`;
  }
}
