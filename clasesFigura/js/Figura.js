class Figura {
	constructor(nombre) {
		if (this.constructor === Figura) {
			throw new TypeError(
				'La clase Abstract Figura no puede ser instanciada directamente.'
			);
		}
		this.nombre = nombre;
	}

	calcularPerimetro() {
		throw new Error('El método calcularPerimetro debe ser implementado.');
	}

	calcularSuperficie() {
		throw new Error('El método calcularSuperficie debe ser implementado.');
	}

	toString() {
		return `<br/>Nombre: ${this.nombre}
		        <br/>Perimetro: ${this.calcularPerimetro()}cm
		        <br/>Superficie: ${this.calcularSuperficie()}cm`;
	}
}
