var myApp = ( function(){

  var foo = 'Module Pattern';
  var bar = 'ver 1.0';

  var sum = function( param1, param2 ){
    return param1 + param2;
  }

  return {
      myMessage: function(){
        return foo + ' ' + bar;
      },
      sum : function( number1, number2 ){
       return sum( number1, number2 )
      }
    }
})();
